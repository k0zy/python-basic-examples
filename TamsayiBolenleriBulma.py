#Coded by
#Baris Dogan


print("INFO\nBitiş değeri: Bölünen sayıların kaça kadar yazılacağıdır.\n")
basla = 1
bitir = int(input("Lütfen bitiş değeri giriniz: "))
a = int(input("1. Tam bölünen sayı: "))
b = int(input("2. Tam bölünen sayı: "))
i = 0 #Döngümüzün kaç defa döndüğünü anlamamız için kullandığımız sayaç değişkeni

for sayac in range(basla,bitir): # basla değişkeninden bitir değişkenine kadar giden for döngüsü
    if((sayac % a == 0) and (sayac % b == 0)):
        print(sayac)
        i += 1
#Kontrol yapısı bitir sayısına kadar sayacın 3 ve 5 e modunun 0'a eşit olduğu zaman çalışıyor


print("1'den ", bitir ,"sayısına kadar ",a," ve ",b," sayılarına bölünen ",i, " tane tamsayı vardır.")

#Coded by
#Baris Dogan
