#Coded by
#Baris Dogan


def menu():
    print("=====================================\n")
    print("1. Celsius'u Fahrenheit'a çevir\n")
    print("2. Fahrenheit'ı Celsius'a çevir\n")
    print("3. Celsius'u Kelvin'e çevir\n")
    print("4. Kelvin'i Celsius'a çevir\n")
    print("0. EXIT\n")
    secim = int(input("Seçim: "))


    while secim != 0:
        if secim == 1:
            print("C to F seçildi")
            cf_degeri = int(input("Celsius değeri giriniz: "))
            cfsonuc = cf_degeri *1.8 +32
            print(cf_degeri,"C =",cfsonuc,"F")
            menu()

        elif secim == 2:
            print("F to C seçildi")
            fc_degeri = int(input("Fahrenheit değeri giriniz: "))
            fc_sonuc = (fc_degeri - 32) /1.8
            print(fc_degeri,"F =",fc_sonuc,"C")
            menu()

        elif secim == 3:
            print("C to K seçildi")
            ck_degeri = int(input("Celsius değeri giriniz: "))
            ck_sonuc = 273.15 + ck_degeri
            print(ck_degeri,"C =",ck_sonuc,"K")
            menu()

        elif secim == 4:
            print("K to C seçildi")
            kc_degeri = int(input("Kelvin değeri giriniz: "))
            kc_sonuc = kc_degeri - 273.15
            print(kc_degeri,"K =",kc_sonuc,"C")
            menu()

        else:
            print("Yanlış değer girdiniz")
            menu()

menu()



#Coded by
#Baris Dogan
