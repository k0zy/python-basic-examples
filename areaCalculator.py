#Geometriye modern terimler katarak matematiğimizi zenginleştiren..
#Cumhuriyetimizin kurucusu Gazi Mustafa Kemal Atatürk'ü minnetle anıyoruz.
#<3<3<3
#Coded by
#Baris Dogan

def main():
    print("Hangi şeklin alanını hesaplamak istiyorsunuz ?\n")
    print("1-Üçgen\n")
    print("2-Dikdörtgen\n")
    print("3-Daire\n")

    secim = int(input("Seçim yapınız (1-2-3) : "))

    if(secim == 1):
        a = int(input("1. Uzunluk değeri: "))
        b = int(input("2. Uzunluk değeri: "))
        print("Üçgeninizin alanı: ",float(a*b)/2)
    elif(secim == 2):
        a = int(input("1. Uzunluk değeri: "))
        b = int(input("2. Uzunluk değeri: "))
        print("Dikdörtgeninizin alanı: ",a*b)
    elif(secim == 3):
        a = int(input("1. Uzunluk değeri: "))
        b = int(input("2. Uzunluk değeri: "))
        print("Dairenizin alanı: ",float(a*a*3.14))
    else:
        print("Lütfen geçerli bir değer giriniz !")

main()

#Coded by
#Baris Dogan