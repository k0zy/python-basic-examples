# Caesar Encrypter / Decrypter
#Encryption: Sifre = Sayacın alfabedeki indexi ile key numarasının toplamı % alfabenin uzunluğunun sayısı
#Decryption: Sifre = Sayacın alfabedeki indexi ile key numarasının farkı % alfabenin uzunluğunun sayısı
#İnternetteki kaynaklardan yararlanılmıştır.
#Coded by
#Baris Dogan

def encryption():
    encrypttext = input("Enter your message: ")
    ek = int(input("Please enter your key (Just numbers): "))
    ealphabet = 'abcçdefgğhıijklmnoöpqrsştuüvwxyz'

    ecipher = ''

    for c in encrypttext:
        if c in ealphabet:
            ecipher += ealphabet[(ealphabet.index(c) + ek) % (len(ealphabet))]

    print("Your encrypted message is:" + ecipher)


def decryption():
    text = input("Enter your crypt: ")
    k = int(input("Please enter your key (Just numbers): "))
    alphabet = 'abcçdefgğhıijklmnoöpqrsştuüvwxyz'

    cipher = ''

    for c in text:
        if c in alphabet:
            cipher += alphabet[(alphabet.index(c) - k) % (len(alphabet))]

    print("Your decrypted message is:" + cipher)



def main():
    choice = int(input("1. Encryption \n 2. Decryption \n ( Choose 1 or 2)"))
    if choice == 1:
        encryption()
    elif choice == 2:
        decryption()
    else:
        print("Wrong choice")

main()


#Coded by
#Baris Dogan
